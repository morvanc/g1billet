# G1Billet

Vous voulez des Ǧ1 sur papier ?

Imprimez vos billets !

Attention, faire des faux ou les laisser circuler est puni par la loi,
et par la perte de confiance de ceux avec qui vous commercez.

## Principe

- Avant de créer un G1Billet, vous transférez le montant correspondant sur un compte porte feuille automatiquement créé pour ce billet.
- Une fois imprimé, vous pouvez vérifier que ce montant est toujours sur le compte via le QR-Code de vérificaiton au recto du billet.
- Pour Encaissez numériquement le billet, vous devrez découper les fragements de QR-Code au verso (et donc détruire le G1billet)
pour reconstituer le QR-Code d'encaissement et l'utiliser pour verser le solde du compte porte feuille du billet vers votre compte.

### Maquette

- Exemple de recto
![recto](recto.png)

- Exemple de verso
![verso](verso.png)

Contrairement aux Ǧ1 numériques, les Ǧ1Billets sont falcifiables.
Pour ne pas rompre la confiance entre être humain,
vérifiez si possible votre billet avant de le donner.
En plus, faire des faux ainsi que les diffuser, même sans le savoir, est puni par la loi.
Enfin, la vérification connecté permet de remonter rapidement au faussaire.

Vous préférez le numérique et sa fiabilité ?
Vous pouvez reconvertir ce Ǧ1Billet en Ǧ1 numériques :
Découpez et assemblez les fragments du QR-code d'encaissement et scannez-le.

Scanner un QR-Code = utiliser un logiciel lecteur de QR-Code depuis une webcam ou un smartphone.

Besoin d'explications ?
- Scan, QR-Code, vérification connecté...
- Comment encaisser ce billet ?
- Comment imprimer vos propres billets ?
- Comment retrouver un faussaire ?

Tout est détaillé dans le projet Ǧ1Billet accessibles aux adresses mirroirs :
- [billet.g1.money](https://billet.g1.money)
- [g1billet.duniter.io](https://g1billet.duniter.io) 


## Fiabilité
Un billet que vous pouvez imprimer vous même, c'est facile à falsifier.
L'alternative est de concentrer le pouvoir d'émission de billet chez un tiers de confiance qui imprimera de coûteux billets infalsifiables (ou difficile à falsifier), et de vérifier que ce tier de confiance n'abuse pas de sa confiance pour faire n'importe quoi.

Du coup, sans tier de confiance, comment limiter la casse ?
- Grâce aux recommandations de vérificiation imprimés sur le billet.
- En rappellant que tromper (avec des faux billets) est puni par la loi (même tromper par négligeance/inadvertance)
- En rendant les faux (par duplication d'un billet) plus voyants grace à l'identicon de chaque billet (visage stylisé unique pour chaque billet)
- En facilitant la vérification de la validité du billet à un instant T grace au QR-code de vérification connecté
- En détectant statistiquement grace aux vérifications connectés les doublons et en le retirant de la circulation
- En remontant aux faussaires en faisant converger les informations des usagers lésé

Le principe d'avoir à découper le billet pour l'encaisser n'empèche pas un utilisateur malveillant de frauder,
mais écarte la diffusion d'un G1Billet après encaissement numérique par inadvertence ou mécompréhension du fonctionnement.

### Qu'est ce qu'un faux G1Billet ?

- Un billet dupliqué (photocopié ou autre procédé)
- Un billet vide (dont le QR-Code de vérification pointe vers un compte vide
que ce soit parce qu'il n'a pas été crédité au départ
ou parcequ'il a été encaissé sans être détruit)

Si vous scannez un faux, il vous sera proposé de déclarer qui vous l'a transmis pour contribuer à l'enquète et remonter jusqu'au faussaire.

### Qu'est ce qu'un vrai G1Billet ?

Un billet unique imprimé recto-verso, associé à un portefeuille unique crédité du montant indiqué sur le G1billet.

## Je veux imprimer des G1Billets maintenant !

Patience, pour l'instant, G1Billet n'est qu'une idée.
Le logiciel viendra !, en attendant, vous pouvez toujours aller jeter un oeil du côté de https://git.duniter.org/tools/paperwallet

## Roadmap

- [ ] [web+g1](https://forum.duniter.org/t/web-g1-clef-publique-ou-autre/4594) syntaxe, page d'enregistrement vers un service
- [ ] lib d'interaction avec la blockchain
  - [ ] consulter le solde d'un compte
  - [ ] transférer d'un compte vers 1 à n comptes
- [ ] [face-identicon](https://1000i100.frama.io/face-identicon/) générateur de visage déterministe ou [jdenticon](https://jdenticon.com/) ou [identicon](http://identicon.net/)
- [ ] page de scan de QR-code pour déclancher les vérifications [barcode-scanner](https://github.com/code-kotis/barcode-scanner) ou [pwa-qr-code-scanner](https://github.com/Minishlink/pwa-qr-code-scanner) (ou approche [react](https://github.com/moaazsidat/react-native-qrcode-scanner)-[native](https://github.com/MarnoDev/AC-QRCode-RN)
- [ ] page de vérification du solde d'un compte `web+g1://is:amount/on:pubkey`
  - [ ] afficher si valide
  - [ ] afficher si invalide + motif (vide, montant invalide (inférieur ou supérieur) + instruction de destruction sinon complicité)
  - [ ] afficher "encaissement numérique recommandé" (selon heuristique à la discrétion du point de vérification)
      - [ ] supérieur à l'écart type du nombre de vérifications par hashIP différente sur les 7 derniers jours
      - [ ] kilométrage entre les lieux de vérification sur les 7 derniers jours supérieur à l'écart type
      - [ ] compte émetteur du G1billet commun avec un compte à l'origine de G1Billet devenu invalides (idem pour les comptes ancètres du compte éméteur)
  - [ ] si le billet provient directement d'un compte membre, affiche "imprimé par {nom cesium} ou {Uid} + {pubKey}"
  - [ ] si un lieu est indiqué dans la transaction de création, affiche "imprimé à {lieu} + distance au lieu actuel + afficher sur une carte"
  - [ ] collecte de statistiques de vérification
      - [ ] nombre de scan valide
      - [ ] nombre de scan invalide
      - [ ] pubkey
      - [ ] hashIP
      - [ ] période (date début, date fin)
      - [ ] lieu du scan (gps) (basé sur ce que le navigateur répond ou a défaut sur du géoip)
      - [ ] portail/site fournisseur de la donnée (pour fédérer les stats entre plusieurs protails)
  - [ ] affichage des statistiques de vérification
      - [ ] % de vérif valide
      - [ ] % de vérif unique valide (pubkey-hashIP unique)
      - [ ] filtre temporel (depuis toujours, de telle date à telle date, dernier mois, semaine, jour...)
      - [ ] filtre géographique (lieu + rayon)
      - [ ] filtre par portail
      - [ ] faux G1billet répandus -> image recto en particulier avec leur face-identicon
- [ ] page de préparation à la création de G1Billets (sélection des billets à créer, du compte à débiter, des dons éventuels, et pourquoi pas du lieu d'émission (stocké dans la transaction))
- [ ] approvisonnement des G1Billets en fabrication
- [ ] génération des images de G1Billet
  - [ ] intégration du QRCode de vérification de validité
  - [ ] intégration du face-identicon
  - [ ] intégration du QRCode d'encaissement fragmenté
  - [ ] intégration du montant
  - [ ] intégration des recommandations d'usage recto et verso
- [ ] génération du pdf avec les G1Billets à imprimer agencés sur feuille A4 recto verso
- [ ] page d'encaissement `web+g1://collect:privatekey`
  - [ ] sélection du compte cible
  - [ ] si commission à l'encaissement (déconseillé) information de l'utilisateur du montant de la commission
  - [ ] encaissement mono-cible (ou multi-cible en cas de commission)
  - [ ] compte rendu d'encaissement.
